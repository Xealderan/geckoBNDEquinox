/**
 * Copyright (c) 2012 - 2019 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.bnd.eclipse.launcher.util;

import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;

import org.eclipse.equinox.launcher.JNIBridge;

public class SplashHandler extends Thread {
		
		private final JNIBridge bridge;
		
		private final AtomicBoolean splashDown = new AtomicBoolean(false);

		private InternalThread internalThread; 
		
		/**
		 * @param thread 
		 * 
		 */
		public SplashHandler(JNIBridge bridge) {
			this.bridge = bridge;
		}

		/**
		 * The splash must be taken up and down by the same Thread. 
		 * 
		 * @author Juergen Albert
		 * @since 16 Oct 2019
		 */
		private static final class InternalThread extends Thread{
			
			private SplashHandler handler;
			private JNIBridge bridge;
			private String splashLocation;
			
			private volatile boolean running = true;
			
			/**
			 * Creates a new instance.
			 */
			public InternalThread(SplashHandler handler, JNIBridge bridge, String splashLocation) {
				this.handler = handler;
				this.bridge = bridge;
				this.splashLocation = splashLocation;
			}
			
			/* 
			 * (non-Javadoc)
			 * @see java.lang.Thread#run()
			 */
			@Override
			public void run() {
				bridge.showSplash(splashLocation);
				try {
					while(running) {}
				} catch (Exception e) {
				}
				System.out.println("Taking down splash");
				handler.takeDownSplash();
			}
			
			public void terminate() {
				running = false;
			}
			
			public void updateSplash() {
				if (this.bridge != null && running) {
					this.bridge.updateSplash();
				}
			}
		}
		
		@Override
		public void run() {
			if(internalThread != null) {
				internalThread.terminate();
			}
		}

		public void updateSplash() {
			internalThread.updateSplash();
		}
			
		public void showsplash( String splashLocation){
			internalThread = new InternalThread(this, bridge, splashLocation);
			Executors.newSingleThreadExecutor().execute(internalThread);
			
		}
		
		/*
		 * Take down the splash screen. 
		 */
		public void takeDownSplash() {
			if (splashDown.get() || this.bridge == null) // splash is already down
				return;
			synchronized (bridge) {
				splashDown.set(this.bridge.takeDownSplash());
			}
			try {
				Runtime.getRuntime().removeShutdownHook(this);
			} catch (Throwable e) {
				// OK to ignore this, happens when the VM is already shutting down
			}
		}
	}