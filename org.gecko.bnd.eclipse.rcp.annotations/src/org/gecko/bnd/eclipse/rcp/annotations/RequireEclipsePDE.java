/**
 * Copyright (c) 2012 - 2019 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.bnd.eclipse.rcp.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.osgi.annotation.bundle.Requirement;

/**
 * 
 * @author Jürgen Albert
 * @since 23.09.2019
 * 	osgi.identity;filter:='(&(osgi.identity=org.eclipse.jdt.annotation)(version>=2.2.300))',\
	bnd.identity;id='org.eclipse.pde.ui',\
	bnd.identity;id='org.eclipse.pde',\
	bnd.identity;id='org.eclipse.pde.build',\
	bnd.identity;id='org.eclipse.pde.core',\
	bnd.identity;id='org.eclipse.pde.genericeditor.extension',\
	bnd.identity;id='org.eclipse.pde.launching',\
	bnd.identity;id='org.eclipse.pde.runtime',\
	bnd.identity;id='org.eclipse.pde.ua.core',\
	bnd.identity;id='org.eclipse.pde.ua.ui',\
 */
@Requirement(namespace="osgi.identity", name="org.eclipse.jdt.annotation")
@Requirement(namespace="osgi.identity", name="org.eclipse.pde.ui")
@Requirement(namespace="osgi.identity", name="org.eclipse.pde.build")
@Requirement(namespace="osgi.identity", name="org.eclipse.pde.genericeditor.extension")
@Requirement(namespace="osgi.identity", name="org.eclipse.pde.core")
@Requirement(namespace="osgi.identity", name="org.eclipse.pde.launching")
@Requirement(namespace="osgi.identity", name="org.eclipse.pde.runtime")
@Requirement(namespace="osgi.identity", name="org.eclipse.pde.ua.core")
@Requirement(namespace="osgi.identity", name="org.eclipse.pde.ua.ui")
@Target(value={ElementType.PACKAGE, ElementType.TYPE})
@Retention(value=RetentionPolicy.CLASS)
@Documented
public @interface RequireEclipsePDE {

}
