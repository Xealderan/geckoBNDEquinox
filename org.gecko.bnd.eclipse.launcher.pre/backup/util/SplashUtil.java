/**
 * Copyright (c) 2012 - 2019 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.bnd.eclipse.launcher.util;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import org.eclipse.equinox.launcher.JNIBridge;
import org.gecko.bnd.eclipse.launcher.pre.EclipseLauncherConstants;

/**
 * A Helper Class containing all the logic handling the splash screen. A lot of the Laugic here originates from the Eclipse Launchers main class.
 * 
 * @author Juergen Albert
 */
public class SplashUtil {

	/*
	 * Handle splash screen.
	 *  The splash screen is displayed natively.  Whether or not the splash screen
	 *  was displayed by the launcher, we invoke JNIBridge.showSplash() and the 
	 *  native code handles the case of the splash screen already existing.
	 * 
	 * The -showsplash argument may indicate the bitmap used by the native launcher,
	 * or the bitmap location may be extracted from the config.ini
	 * 
	 * We pass a handler (Runnable) to the platform which is called as a result of the
	 * launched application calling Platform.endSplash(). This handle calls 
	 * JNIBridge.takeDownSplash and the native code will close the splash screen.
	 * 
	 * The -endsplash argument is longer used and has the same result as -nosplash
	 * 
	 * @param defaultPath search path for the boot plugin
	 */
	public static SplashHandler handleSplash(EclipseLauncherConstants props, List<URL> classpath,  JNIBridge bridge) {
		// run without splash if we are initializing or nosplash 
		// was specified (splashdown = true)
		if (props.initialize || props.splashDown || bridge == null) {
			props.showSplash = false;
			props.endSplash = null;
			return null;
		}

		SplashHandler splashHandler = new SplashHandler(props, bridge);
		
		if (props.showSplash || props.endSplash != null) {
			// Register the endSplashHandler to be run at VM shutdown. This hook will be 
			// removed once the splash screen has been taken down.
			try {
				Runtime.getRuntime().addShutdownHook(splashHandler);
			} catch (Throwable ex) {
				// Best effort to register the handler
			}
		}

		// if -endsplash is specified, use it and ignore any -showsplash command
		if (props.endSplash != null) {
			props.showSplash = false;
			return null;
		}

		// check if we are running without a splash screen
		if (!props.showSplash)
			return null;

		// determine the splash location
		props.splashLocation = getSplashLocation(classpath, props);
		if (props.debug)
			CommonUtil.log(SplashUtil.class, "Splash location: %s ", props.splashLocation); //$NON-NLS-1$
		if (props.splashLocation == null)
			return null;

//		CountDownLatch latch = new CountDownLatch(1);
//		try {
//			latch.await(2000, TimeUnit.MILLISECONDS);
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		
		bridge.showSplash(props.splashLocation);
		
		File f = new File(props.splashLocation);
		if(!f.exists()) {
			CommonUtil.log(SplashUtil.class, "Splash does not exist"); //$NON-NLS-1$
		}
		
		long handle = bridge.getSplashHandle();
		if (handle != 0 && handle != -1) {
			System.getProperties().put(EclipseLauncherConstants.SPLASH_HANDLE, String.valueOf(handle));
			System.getProperties().put(EclipseLauncherConstants.SPLASH_LOCATION, props.splashLocation);
			bridge.updateSplash();
			return splashHandler;
		} else {
			// couldn't show the splash screen for some reason
			props.splashDown = true;
			CommonUtil.log(SplashUtil.class, "Couldn't load Splash for some unknown reason");
		}
		return null;
	}
	
	/*
	 * Return path of the splash image to use.  First search the defined splash path.
	 * If that does not work, look for a default splash.  Currently the splash must be in the file system
	 * so the return value here is the file system path.
	 */
	public static String getSplashLocation(List<URL> bootPath, EclipseLauncherConstants props) {
		//check the path passed in from -showsplash first.  The old launcher passed a timeout value
		//as the argument, so only use it if it isn't a number and the file exists.
		if (props.splashLocation != null && new File(props.splashLocation).exists()) {
			System.setProperty(EclipseLauncherConstants.PROP_SPLASHLOCATION, props.splashLocation);
			return props.splashLocation;
		}
		String result = System.getProperty(EclipseLauncherConstants.PROP_SPLASHLOCATION);
		if (result != null)
			return result;
		String splashPath = props.splashPath;
		if (splashPath != null) {
			String[] entries = CommonUtil.getArrayFromList(splashPath);
			ArrayList<String> path = new ArrayList<>(entries.length);
			for (int i = 0; i < entries.length; i++) {
				String entry = CommonUtil.resolve(entries[i], props);
				if (entry != null && entry.startsWith("file:")) {
					File entryFile = new File(entry.substring(5).replace('/', File.separatorChar));
					entry = CommonUtil.searchFor(entryFile.getName(), entryFile.getParent());
					if (entry != null)
						path.add(entry);
				} else {
					CommonUtil.log(SplashUtil.class, "Invalid splash path entry: %s", entries[i]); //$NON-NLS-1$
				}
			}
			// see if we can get a splash given the splash path
			result = searchForSplash(path.toArray(new String[path.size()]), props);
			if (result != null) {
				System.setProperty(EclipseLauncherConstants.PROP_SPLASHLOCATION, result);
				return result;
			}
		}
		return result;
	}
	
	/*
	 * Do a locale-sensitive lookup of splash image
	 */
	private static String searchForSplash(String[] searchPath, EclipseLauncherConstants props) {
		if (searchPath == null)
			return null;

		// Get the splash screen for the specified locale
		String locale = props.nl;
		if (locale == null)
			locale = Locale.getDefault().toString();
		String[] nlVariants = buildNLVariants(locale);

		for (int i = 0; i < nlVariants.length; i++) {
			for (int j = 0; j < searchPath.length; j++) {
				String path = searchPath[j];
				if (path.startsWith(EclipseLauncherConstants.FILE_SCHEME))
					path = path.substring(5);
				// do we have a JAR?
				if (CommonUtil.isJAR(path)) {
					String result = CommonUtil.extractFromJAR(path, nlVariants[i], props);
					if (result != null)
						return result;
				} else {
					// we have a file or a directory
					if (!path.endsWith(File.separator))
						path += File.separator;
					path += nlVariants[i];
					File result = new File(path);
					if (result.exists())
						return result.getAbsolutePath(); // return the first match found [20063]
				}
			}
		}

		// sorry, could not find splash image
		return null;
	}
	
	/*
	 * Build an array of path suffixes based on the given NL which is suitable
	 * for splash path searching.  The returned array contains paths in order 
	 * from most specific to most generic. So, in the FR_fr locale, it will return 
	 * "nl/fr/FR/splash.bmp", then "nl/fr/splash.bmp", and finally "splash.bmp". 
	 * (we always search the root)
	 */
	private static String[] buildNLVariants(String locale) {
		//build list of suffixes for loading resource bundles
		String nl = locale;
		ArrayList<String> result = new ArrayList<>(4);
		int lastSeparator;
		while (true) {
			result.add("nl" + File.separatorChar + nl.replace('_', File.separatorChar) + File.separatorChar + EclipseLauncherConstants.SPLASH_IMAGE); //$NON-NLS-1$
			lastSeparator = nl.lastIndexOf('_');
			if (lastSeparator == -1)
				break;
			nl = nl.substring(0, lastSeparator);
		}
		//add the empty suffix last (most general)
		result.add(EclipseLauncherConstants.SPLASH_IMAGE);
		return result.toArray(new String[result.size()]);
	}
}
