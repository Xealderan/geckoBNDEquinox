/**
 * Copyright (c) 2012 - 2019 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.eclipse.core.supplement.localization;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Dictionary;
import java.util.Enumeration;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.concurrent.ConcurrentHashMap;

import org.osgi.framework.Bundle;
import org.osgi.framework.Constants;

/**
 * A resource bundle cache for an OSGi bundle. Currently it just supports properties.
 * @author Mark Hoffmann
 * @since 07.11.2019
 */
public class ResourceBundleCache {
	
	private final Bundle bundle;
	private final Map<String, BundleResourceBundle> resourceBundleCache = new ConcurrentHashMap<String, BundleResourceBundle>();
	
	public ResourceBundleCache(Bundle bundle) {
		this.bundle = bundle;
	}
	
	public Bundle getBundle() {
		return bundle;
	}
	
	/**
	 * Returns a resource bundle for the given locale
	 * @param localeString the locale string
	 * @param isDefaultLocale <code>true</code>, if the given local is the default locale
	 * @return
	 */
	public ResourceBundle getResourceBundle(String localeString, boolean isDefaultLocale) {
		BundleResourceBundle resourceBundle = lookupResourceBundle(localeString);
		if (isDefaultLocale)
			return (ResourceBundle) resourceBundle;
		// need to determine if this is resource bundle is an empty stem
		// if it is then the default locale should be used
		if (resourceBundle == null || resourceBundle.isStemEmpty())
			return (ResourceBundle) lookupResourceBundle(Locale.getDefault().toString());
		return (ResourceBundle) resourceBundle;
	}
	
	private BundleResourceBundle lookupResourceBundle(String localeString) {
		// get the localization header as late as possible to avoid accessing the raw headers
		// getting the first value from the raw headers forces the manifest to be parsed (bug 332039)
		Dictionary<String, String> manifest = bundle.getHeaders();
		String localizationHeader = manifest.get(Constants.BUNDLE_LOCALIZATION);
		if (localizationHeader == null)
			localizationHeader = Constants.BUNDLE_LOCALIZATION_DEFAULT_BASENAME;

		BundleResourceBundle result = resourceBundleCache.get(localeString);
		if (result != null)
			return result.isEmpty() ? null : result;

		// Collect all the necessary inputstreams to create the resource bundle without
		// holding any locks.  Finding resources and inputstreams from the wirings requires a
		// read lock on the module database.  We must not hold the cache lock while doing this;
		// otherwise out of order locks will be possible when the resolver needs to clear the cache
		String[] nlVarients = buildNLVariants(localeString);
		InputStream[] nlStreams = new InputStream[nlVarients.length];
		for (int i = nlVarients.length - 1; i >= 0; i--) {

			URL url = findResource(localizationHeader + (nlVarients[i].equals("") ? nlVarients[i] : '_' + nlVarients[i]) + ".properties"); //$NON-NLS-1$ //$NON-NLS-2$
			if (url != null) {
				try {
					nlStreams[i] = url.openStream();
				} catch (IOException e) {
					// ignore
				}
			}
		}

		synchronized (resourceBundleCache) {
			BundleResourceBundle parent = null;
			for (int i = nlVarients.length - 1; i >= 0; i--) {
				BundleResourceBundle varientBundle = null;
				InputStream varientStream = nlStreams[i];
				if (varientStream == null) {
					varientBundle = resourceBundleCache.get(nlVarients[i]);
				} else {
					try {
						varientBundle = new LocalizationResourceBundle(varientStream);
					} catch (IOException e) {
						// ignore and continue
					} finally {
						if (varientStream != null) {
							try {
								varientStream.close();
							} catch (IOException e3) {
								//Ignore exception
							}
						}
					}
				}

				if (varientBundle == null) {
					varientBundle = new EmptyResourceBundle(nlVarients[i]);
				}
				if (parent != null)
					varientBundle.setParent((ResourceBundle) parent);
				resourceBundleCache.put(nlVarients[i], varientBundle);
				parent = varientBundle;
			}
			result = resourceBundleCache.get(localeString);
			return result.isEmpty() ? null : result;
		}
	}
	
	private String[] buildNLVariants(String nl) {
		List<String> result = new ArrayList<>();
		while (nl.length() > 0) {
			result.add(nl);
			int i = nl.lastIndexOf('_');
			nl = (i < 0) ? "" : nl.substring(0, i); //$NON-NLS-1$
		}
		result.add(""); //$NON-NLS-1$
		return result.toArray(new String[result.size()]);
	}
	
	private URL findResource(String resource) {
		if (bundle != null) {
			int lastSlash = resource.lastIndexOf('/');
			String path = lastSlash > 0 ? resource.substring(0, lastSlash) : "/"; //$NON-NLS-1$
			String fileName = lastSlash != -1 ? resource.substring(lastSlash + 1) : resource;
			Enumeration<URL> result = bundle.findEntries(path, fileName, true);
			return result == null || !result.hasMoreElements() ? null : result.nextElement();
		}
		return  null;
	}

}
