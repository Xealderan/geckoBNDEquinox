/**
 * Copyright (c) 2012 - 2019 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.bnd.eclipse.launcher.util;

import org.eclipse.equinox.launcher.JNIBridge;
import org.gecko.bnd.eclipse.launcher.pre.EclipseLauncherConstants;

public class SplashHandler extends Thread {
		
		private final EclipseLauncherConstants props;
		private final JNIBridge bridge;
		
		/**
		 * 
		 */
		public SplashHandler(EclipseLauncherConstants properties, JNIBridge bridge) {
			this.props = properties;
			this.bridge = bridge;
		}
		
		@Override
		public void run() {
			takeDownSplash();
		}

		public void updateSplash() {
			if (this.bridge != null && !props.splashDown) {
				this.bridge.updateSplash();
			}
		}
		
		/*
		 * Take down the splash screen. 
		 */
		public void takeDownSplash() {
			if (this.props.splashDown || this.bridge == null) // splash is already down
				return;

			this.props.splashDown = this.bridge.takeDownSplash();
			System.clearProperty(EclipseLauncherConstants.SPLASH_HANDLE);

			try {
				Runtime.getRuntime().removeShutdownHook(this);
			} catch (Throwable e) {
				// OK to ignore this, happens when the VM is already shutting down
			}
		}
	}