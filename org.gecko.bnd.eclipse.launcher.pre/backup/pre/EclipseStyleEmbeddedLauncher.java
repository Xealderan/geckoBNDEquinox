/**
 * Copyright (c) 2012 - 2019 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.bnd.eclipse.launcher.pre;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.invoke.MethodHandle;
import java.lang.invoke.MethodHandles;
import java.lang.invoke.MethodType;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.List;
import java.util.jar.Manifest;
import java.util.stream.Collectors;

import org.eclipse.equinox.launcher.JNIBridge;
import org.gecko.bnd.eclipse.launcher.util.CommonUtil;
import org.gecko.bnd.eclipse.launcher.util.SplashHandler;
import org.gecko.bnd.eclipse.launcher.util.SplashUtil;

import aQute.lib.io.IOConstants;

public class EclipseStyleEmbeddedLauncher {

	/**
	 * 
	 */
	private static final String ORG_GECKO_BND_ECLIPSE_LAUNCHER_LAUNCHER = "org.gecko.bnd.eclipse.launcher.Launcher";
	private static final String BND_LAUNCHER = "aQute.launcher.Launcher";
	static final int			BUFFER_SIZE			= IOConstants.PAGE_SIZE * 16;

	public static final String	EMBEDDED_RUNPATH	= "Embedded-Runpath";
	public static Manifest		MANIFEST;
	private static JNIBridge bridge;
	private static SplashHandler splashHandler;

	public static void main(String[] args) throws Throwable {

		boolean isVerbose = isTrace();

		findAndExecute(isVerbose, "main", void.class, args);
	}

	/**
	 * Runs the Launcher like the main method, but returns an usable exit Code.
	 * This Method was introduced to enable compatibility with the Equinox
	 * native executables.
	 * 
	 * @param args the arguments to run the Launcher with
	 * @return an exit code
	 * @throws Throwable
	 */
	public int run(String... args) throws Throwable {

		boolean isVerbose = isTrace();

		if (isVerbose) {
			log("The following arguments are given:");
			for (String arg : args) {
				log(arg);
			}
		}

		String methodName = "run";
		Class<Integer> returnType = int.class;

		return findAndExecute(isVerbose, methodName, returnType, args);
	}

	/**
	 * @param isVerbose should we log debug messages
	 * @param methodName the method name to look for
	 * @param returnType the expected return type
	 * @param args the arguments for the method
	 * @return what ever the method returns
	 * @throws Throwable
	 */
	private static <T> T findAndExecute(boolean isVerbose, String methodName, Class<T> returnType, String... args)
		throws Throwable, InvocationTargetException {
		ClassLoader cl = EclipseStyleEmbeddedLauncher.class.getClassLoader();

		EclipseLauncherConstants props = new EclipseLauncherConstants(args);

		//the launcher constants remove some constants. We have to do the following to avoid null values
		args = Arrays.asList(args).stream().filter(s -> s != null).collect(Collectors.toList()).toArray(new String[0]);
		
		if (isVerbose) {
			log("The following arguments after props:");
			for (String arg : args) {
				System.out.println(arg);
			}
		}
		if(props.library != null) {
			if (isVerbose)
				log("linking library %s", props.library);
			
			bridge = setupJNI(props);
			if(bridge == null) {
				error("No native launcher lib found at %s", props.library);
			}
		}
		
		List<URL> classpath = new ArrayList<>();
		
		if (isVerbose)
			log("looking for + " + EMBEDDED_RUNPATH + " in META-INF/MANIFEST.MF");
		
		Enumeration<URL> manifests = cl.getResources("META-INF/MANIFEST.MF");
		while (manifests.hasMoreElements()) {
			URL murl = manifests.nextElement();

			if (isVerbose)
				log("found a Manifest %s", murl.toString());

			Manifest m = new Manifest(murl.openStream());
			String runpath = m.getMainAttributes()
				.getValue(EMBEDDED_RUNPATH);
			if (isVerbose)
				log("Going through the following runpath %s", runpath);
			if (runpath != null) {
				MANIFEST = m;
				
				for (String path : runpath.split("\\s*,\\s*")) {
					URL url = toFileURL(cl.getResource(path));
					if (isVerbose)
						log("Adding to classpath %s", url.toString());
					classpath.add(url);
				}
				break;
			}
		} 
		
		addPropertyBasedRunPath(classpath, props);

		if (isVerbose)
			log("creating URLClassLoader");
		try (URLClassLoader urlc = new URLClassLoader(classpath.toArray(new URL[0]), cl)) {
			
			log("Try to load %s", ORG_GECKO_BND_ECLIPSE_LAUNCHER_LAUNCHER);
			
			Class<?> launcher = urlc.loadClass(ORG_GECKO_BND_ECLIPSE_LAUNCHER_LAUNCHER);
			if(launcher == null) {
				throw new RuntimeException("Found Nothing to launch. Maybe no " + EMBEDDED_RUNPATH + " was set");
			}
			if (isVerbose)
				log("looking for method %s with return type %s and %s capable of handling the splash", methodName, returnType.toString(), Runnable.class.getName());
			MethodHandle mh = MethodHandles.publicLookup()
				.findStatic(launcher, methodName, MethodType.methodType(returnType, String[].class, Runnable.class));
			boolean doSplash = false;
			if(bridge != null && mh != null) {
				doSplash = true;
				if(isVerbose) {
					log("Looking for a SplashScreen");
				}
				if(props.splashLocation != null) {
					splashHandler = SplashUtil.handleSplash(props, classpath, bridge);
				} else {
					if(isVerbose) {
						log("No Splashscreen location set");
					}
				}
			}
			if(!doSplash) {
				if (isVerbose)
					log("looking for method %s with return type %s", methodName, returnType.toString());
				mh = MethodHandles.publicLookup()
						.findStatic(launcher, methodName, MethodType.methodType(returnType, String[].class));
			}
			
			if(mh == null) {
				throw new RuntimeException("Found Nothing to launch. Maybe no " + EMBEDDED_RUNPATH + " was set");
			}
			try {
				if (isVerbose)
					log("found method and start executing with args " );
				if(doSplash) {
					return (T) mh.invoke(args, splashHandler);
				} else {
					return (T) mh.invoke(args);
				}
			} catch (Error | Exception e) {
				throw e;
			} catch (Throwable e) {
				throw new InvocationTargetException(e);
			} finally {
				if(splashHandler != null) {
					splashHandler.takeDownSplash();
				}
				if (bridge != null)
					bridge.uninitialize();
			}
		} 
	}
	
	/**
	 * @param classpath
	 */
	private static void addPropertyBasedRunPath(List<URL> classpath, EclipseLauncherConstants props) {
		props.propBasedRunPath.stream().map(pathElement -> convertToBundleUrl(pathElement, props)).map(url -> {
			log("Adding to classpath %s", url);
			return url;
		}
		).filter(u -> u != null).forEach(classpath::add);
	}

	private static URL convertToBundleUrl(String path, EclipseLauncherConstants props) {
		File f = new File(path);
		try {
			if(f.exists()) {
				return f.toURI().toURL();
			} else {
				//Mybe it is a relative Path
				f = new File(props.installationLocation + path);
				if(f.exists()) {
					return f.toURI().toURL();
				}
			}
		} catch (Exception e) {
			error("Could not convert path %s to URL. Message was %s", path, e.getMessage());
		}
		return null;
	}
	
	/**
	 *  Sets up the JNI bridge to native calls
	 */
	private static JNIBridge setupJNI(EclipseLauncherConstants props) {

		String library = props.library;
		if (library != null) {
			File lib = new File(library);
			if (lib.exists()) {
				JNIBridge bridge = new JNIBridge(lib.getAbsolutePath());
				bridge.setLauncherInfo(System.getProperty(EclipseLauncherConstants.PROP_LAUNCHER), System.getProperty(EclipseLauncherConstants.PROP_LAUNCHER_NAME));
				return bridge;
			}
		}
		return null;
	}
	


	
	
//	private String getLibraryPath(String fragmentName, URL[] defaultPath) {
//		String libPath = null;
//		String fragment = null;
//		if (inDevelopmentMode && devClassPathProps != null) {
//			String devPathList = devClassPathProps.getProperty(PLUGIN_ID);
//			String[] locations = getArrayFromList(devPathList);
//			if (locations.length > 0) {
//				File location = new File(locations[0]);
//				if (location.isAbsolute()) {
//					String dir = location.getParent();
//					fragment = searchFor(fragmentName, dir);
//					if (fragment != null)
//						libPath = getLibraryFromFragment(fragment);
//				}
//			}
//		}
//		if (libPath == null && bootLocation != null) {
//			URL[] urls = defaultPath;
//			if (urls != null && urls.length > 0) {
//				//the last one is most interesting
//				for (int i = urls.length - 1; i >= 0 && libPath == null; i--) {
//					File entryFile = new File(urls[i].getFile());
//					String dir = entryFile.getParent();
//					if (inDevelopmentMode) {
//						String devDir = dir + "/" + PLUGIN_ID + "/fragments"; //$NON-NLS-1$ //$NON-NLS-2$
//						fragment = searchFor(fragmentName, devDir);
//					}
//					if (fragment == null)
//						fragment = searchFor(fragmentName, dir);
//					if (fragment != null)
//						libPath = getLibraryFromFragment(fragment);
//				}
//			}
//		}
//		if (libPath == null) {
//			URL install = getInstallLocation();
//			String location = install.getFile();
//			location += "/plugins/"; //$NON-NLS-1$
//			fragment = searchFor(fragmentName, location);
//			if (fragment != null)
//				libPath = getLibraryFromFragment(fragment);
//		}
//		return libPath;
//	}

//	private String getLibraryFromFragment(String fragment) {
//		if (fragment.startsWith(FILE_SCHEME))
//			fragment = fragment.substring(5);
//
//		File frag = new File(fragment);
//		if (!frag.exists())
//			return null;
//
//		if (frag.isDirectory())
//			return searchFor("eclipse", fragment); //$NON-NLS-1$;
//
//		ZipFile fragmentJar = null;
//		try {
//			fragmentJar = new ZipFile(frag);
//		} catch (IOException e) {
//			log("Exception opening JAR file: " + fragment); //$NON-NLS-1$
//			log(e);
//			return null;
//		}
//
//		Enumeration<? extends ZipEntry> entries = fragmentJar.entries();
//		String entry = null;
//		while (entries.hasMoreElements()) {
//			ZipEntry zipEntry = entries.nextElement();
//			if (zipEntry.getName().startsWith("eclipse_")) { //$NON-NLS-1$
//				entry = zipEntry.getName();
//				try {
//					fragmentJar.close();
//				} catch (IOException e) {
//					//ignore
//				}
//				break;
//			}
//		}
//		if (entry != null) {
//			String lib = extractFromJAR(fragment, entry);
//			if (!getOS().equals("win32")) { //$NON-NLS-1$
//				try {
//					Runtime.getRuntime().exec(new String[] {"chmod", "755", lib}).waitFor(); //$NON-NLS-1$ //$NON-NLS-2$
//				} catch (Throwable e) {
//					//ignore
//				}
//			}
//			return lib;
//		}
//		return null;
//	}
	
	private static void log(String message, Object... args) {
		CommonUtil.log(EclipseStyleEmbeddedLauncher.class, message, args);
	}

	private static void error(String message, Object... args) {
		CommonUtil.error(EclipseStyleEmbeddedLauncher.class, message, args);
	}

	private static boolean isTrace() {
		return Boolean.getBoolean(EclipseLauncherConstants.LAUNCH_TRACE);
	}

	private static URL toFileURL(URL resource) throws IOException {
		//
		// Don't bother copying file urls
		//
		if (resource.getProtocol()
			.equalsIgnoreCase("file"))
			return resource;

		//
		// Need to make a copy to a temp file
		//

		File f = File.createTempFile("resource", ".jar");
		Files.createDirectories(f.getParentFile()
			.toPath());
		try (InputStream in = resource.openStream(); OutputStream out = Files.newOutputStream(f.toPath())) {
			byte[] buffer = new byte[BUFFER_SIZE];
			for (int size; (size = in.read(buffer, 0, buffer.length)) > 0;) {
				out.write(buffer, 0, size);
			}
		}
		f.deleteOnExit();
		return f.toURI()
			.toURL();
	}

}
