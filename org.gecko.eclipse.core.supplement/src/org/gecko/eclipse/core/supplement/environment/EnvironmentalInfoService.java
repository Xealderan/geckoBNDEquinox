/**
 * Copyright (c) 2012 - 2019 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.eclipse.core.supplement.environment;

import org.eclipse.osgi.service.environment.EnvironmentInfo;
import org.osgi.service.component.annotations.Component;

/**
 * Implementation for the {@link EnvironmentInfo}
 * @author Mark Hoffmann
 * @since 07.11.2019
 */
@Component(immediate=true)
public class EnvironmentalInfoService implements EnvironmentInfo{
	
	/* 
	 * (non-Javadoc)
	 * @see org.eclipse.osgi.service.environment.EnvironmentInfo#getCommandLineArgs()
	 */
	@Override
	public String[] getCommandLineArgs() {
		return new String[0];
	}

	/* 
	 * (non-Javadoc)
	 * @see org.eclipse.osgi.service.environment.EnvironmentInfo#getFrameworkArgs()
	 */
	@Override
	public String[] getFrameworkArgs() {
		return new String[0];
	}

	/* 
	 * (non-Javadoc)
	 * @see org.eclipse.osgi.service.environment.EnvironmentInfo#getNL()
	 */
	@Override
	public String getNL() {
		return "de_de";
	}

	/* 
	 * (non-Javadoc)
	 * @see org.eclipse.osgi.service.environment.EnvironmentInfo#getNonFrameworkArgs()
	 */
	@Override
	public String[] getNonFrameworkArgs() {
		return new String[0];
	}

	/* 
	 * (non-Javadoc)
	 * @see org.eclipse.osgi.service.environment.EnvironmentInfo#getOS()
	 */
	@Override
	public String getOS() {
		return "linux";
	}

	/* 
	 * (non-Javadoc)
	 * @see org.eclipse.osgi.service.environment.EnvironmentInfo#getOSArch()
	 */
	@Override
	public String getOSArch() {
		return "x86_64";
	}

	/* 
	 * (non-Javadoc)
	 * @see org.eclipse.osgi.service.environment.EnvironmentInfo#getProperty(java.lang.String)
	 */
	@Override
	public String getProperty(String arg0) {
		return null;
	}

	/* 
	 * (non-Javadoc)
	 * @see org.eclipse.osgi.service.environment.EnvironmentInfo#getWS()
	 */
	@Override
	public String getWS() {
		return "gtk";
	}

	/* 
	 * (non-Javadoc)
	 * @see org.eclipse.osgi.service.environment.EnvironmentInfo#inDebugMode()
	 */
	@Override
	public boolean inDebugMode() {
		return false;
	}

	/* 
	 * (non-Javadoc)
	 * @see org.eclipse.osgi.service.environment.EnvironmentInfo#inDevelopmentMode()
	 */
	@Override
	public boolean inDevelopmentMode() {
		// TODO Auto-generated method stub
		return false;
	}

	/* 
	 * (non-Javadoc)
	 * @see org.eclipse.osgi.service.environment.EnvironmentInfo#setProperty(java.lang.String, java.lang.String)
	 */
	@Override
	public String setProperty(String arg0, String arg1) {
		return null;
	}

	// TODO: class provided by template

}
