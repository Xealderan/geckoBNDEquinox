/**
 * Copyright (c) 2012 - 2019 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.eclipse.core.supplement.localization;

import java.io.IOException;
import java.io.InputStream;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

/**
 * Necessary implementation of a {@link PropertyResourceBundle}
 * @author Mark Hoffmann
 * @since 07.11.2019
 */
public class LocalizationResourceBundle extends PropertyResourceBundle implements BundleResourceBundle {

	public LocalizationResourceBundle(InputStream in) throws IOException {
		super(in);
	}

	public void setParent(ResourceBundle parent) {
		super.setParent(parent);
	}

	public boolean isEmpty() {
		return false;
	}

	public boolean isStemEmpty() {
		return parent == null;
	}
	
}