/**
 * Copyright (c) 2012 - 2019 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.bnd.equinox.launcher.splashscreen;

import java.io.File;
import java.util.Hashtable;

import org.eclipse.equinox.launcher.JNIBridge;
import org.eclipse.osgi.service.runnable.StartupMonitor;
import org.gecko.bnd.eclipse.launcher.util.DefaultStartupMonitor;
import org.gecko.bnd.eclipse.launcher.util.SplashHandler;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceRegistration;

/**
 * 
 * @author Juergen Albert
 * @since 30 Sep 2019
 */
public class SplashScreenActivator implements BundleActivator {

	public static final String IMMEDIATE = "AFTER_FRAMEWORK_INIT";
	
	private SplashHandler splashHandler;
	private JNIBridge bridge;

	private ServiceRegistration<Runnable> splashHandlerRegistry;

	private ServiceRegistration<StartupMonitor> startupMonitorRegistration;

	/* 
	 * (non-Javadoc)
	 * @see org.osgi.framework.BundleActivator#start(org.osgi.framework.BundleContext)
	 */
	@Override
	public void start(BundleContext context) throws Exception {
		System.out.println("Pulling up Splash");
		String launcherLib = context.getProperty("launcher.library");
		System.out.println("launcher lib " + launcherLib);
		if(launcherLib != null) {
			bridge = new JNIBridge(launcherLib);
			String splashLocation = context.getProperty("splash.location");
			if(splashLocation != null) {
//			String realSplashLocation = getSplashLocation(splashLocation);
				String realSplashLocation = splashLocation;
				
				File f = new File(realSplashLocation);
				if(!f.exists()) {
					System.out.println("Splash does not exist at " + f.getAbsolutePath()); //$NON-NLS-1$
					return;
				}
				
				if(realSplashLocation != null) {
					splashHandler = new SplashHandler(bridge);
					
					// determine the splash location
//					bridge.showSplash(realSplashLocation);
					splashHandler.showsplash(realSplashLocation);
					splashHandlerRegistry = context.registerService(Runnable.class, splashHandler, new Hashtable<String, String>());
					startupMonitorRegistration = context.registerService(StartupMonitor.class, new DefaultStartupMonitor(splashHandler), new Hashtable<String, String>());
					
					
					// Register the endSplashHandler to be run at VM shutdown. This hook will be 
					// removed once the splash screen has been taken down.
					try {
						Runtime.getRuntime().addShutdownHook(splashHandler);
					} catch (Throwable ex) {
						// Best effort to register the handler
					}
				}
			}
		}
	}

	/* 
	 * (non-Javadoc)
	 * @see org.osgi.framework.BundleActivator#stop(org.osgi.framework.BundleContext)
	 */
	@Override
	public void stop(BundleContext context) throws Exception {
		if(splashHandler != null) {
			splashHandler.run();
			if(splashHandlerRegistry != null) {
				splashHandlerRegistry.unregister();
				splashHandlerRegistry = null;
			}
			if(startupMonitorRegistration != null) {
				startupMonitorRegistration.unregister();
				startupMonitorRegistration = null;
			}
		}

	}

}
