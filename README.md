# Gecko BND RCP Exporter/Launcher

This aims toward enabling pur OSGi Projects and or bnd to better work with different specialties of Eclipse Equinox and the Eclipse RCP Framework. This entails:

* A BND Plugin, that allows product exports that use the native equinox launcher
* Splashscreen support
* In Framework start of the Eclipse Product/Application

!Please Note: This is explicitly no exact Product Export as the PDE would produce. The original Product export and Eclipse/Equinox start mechanism has a lot of non standard idiosyncrasies that have largely historic reasons and are nowadays unnecessary. Thus this product export works a bit different from what you currently know.! 

Everything described here will target the BND Workspace model. It should work with the maven model as well, but is untested at this point.

# HowTo

## Repositories

All Resources are provided as Maven Nexus and OBR repositories.   

# BND Enablement ToDo List

## GeckoBNDEquinox
* Move the launcher library property from the config.ini to the runproperties
* Implement deflation of Eclipse-Bundleshape dir

## Equinox
* Provide native launcher additionally in a jar (Bog opened)
* remove the need for the JNIBridge in the pre launcher (Bug opened)
* provide capabilities for the native libraries and native launchers

## BND
* Fully Integrate our Project Launcher
* Support deflated Bundles in the Indexer
* Pack deflated jars on download if possible (need to check how p2 is doing this)

# Gecko Eclipse Core Supplement

If you want to use the Equinox registry without using the Equinox OSGi implementation, there is the bundle called *org.eclipse.equinox.supplement*. It bundles certain sources from the Equinox implementation, without having an OSGi framework.

The *org.gecko.eclipse.core.supplement* follows this approach and provides base services that are needed to get *org.eclipse.core* stuff running. In detail this is currently:

* **org.eclipse.osgi.environment.EnvironmentInfo** Service 
* **org.eclipse.osgi.service.localization.BundleLocalization** Service

Both interfaces are provided by either the  *org.eclipse.osgi* or *org.eclips.equinox.supplement* bundle.

To take profit of this extension you currently need modified version of some  bundles, mainly to avoid unnecessary dependencies.

## org.gecko.eclipse.core.supplement

This is an extension that provides the following services: 

* **org.eclipse.osgi.environment.EnvironmentInfo** Service 
* **org.eclipse.osgi.service.localization.BundleLocalization** Service

The *EnvironmentInfo* service is just an empty facade without deeper function. 

The *BundleLocalization* implementation is based upon that one from *org.eclipse.osgi.storage.BundleLocalizationImpl*. But the approach is more OSGi native and does not use Equinox internal stuff. This service is used to find property files for bundle translations. This service is working behind the *Platform#getResourceBundle* call in *org.eclipse.core.runtime*.

Nearly all EMF related projects use that.

To take profit of this extension you currently need modified version of some bundles, mainly to avoid unnecessary dependencies and hard links to the Equinox OSGi framework.

## org.eclipse.core.runtime

This bundle hard wires the Equinox OSGi framework as bundle requirement. That forces to use the Equinox OSGi implementation, whenever you need *org.eclipse.core.runtime*.

If you change the manifest and remove the require bundle declaration and use import packages instead, you open this bundle to run on other OSGi frameworks as well.

**Before**:

```
Require-Bundle: org.eclipse.osgi;bundle-version="[3.13.0,4.0.0)";visibility:=reexport,
 org.eclipse.equinox.common;bundle-version="[3.10.0,4.0.0)";visibility:=reexport,
 org.eclipse.core.jobs;bundle-version="[3.10.0,4.0.0)";visibility:=reexport,
 org.eclipse.equinox.registry;bundle-version="[3.8.0,4.0.0)";visibility:=reexport,
 org.eclipse.equinox.preferences;bundle-version="[3.7.0,4.0.0)";visibility:=reexport,
 org.eclipse.core.contenttype;bundle-version="[3.7.0,4.0.0)";visibility:=reexport,
 org.eclipse.equinox.app;bundle-version="1.3.0";visibility:=reexport
```



**After:**

```
Require-Bundle: org.eclipse.equinox.common;bundle-version="[3.10.0,4.0.0)";visibility:=reexport,
 org.eclipse.core.jobs;bundle-version="[3.10.0,4.0.0)";visibility:=reexport,
 org.eclipse.equinox.registry;bundle-version="[3.8.0,4.0.0)";visibility:=reexport,
 org.eclipse.equinox.preferences;bundle-version="[3.7.0,4.0.0)";visibility:=reexport,
 org.eclipse.core.contenttype;bundle-version="[3.7.0,4.0.0)";visibility:=reexport,
 org.eclipse.equinox.app;bundle-version="1.3.0";visibility:=reexport
...
Import-Package: org.eclipse.equinox.log;version="1.1.0",
 org.eclipse.osgi.container;version="1.3.0",
 org.eclipse.osgi.framework.log;version="1.1.0",
 org.eclipse.osgi.service.datalocation;version="1.3.0",
 org.eclipse.osgi.service.debug;version="1.2.0",
 org.eclipse.osgi.service.environment;version="1.3.0",
 org.eclipse.osgi.service.resolver;version="1.6.0",
 org.eclipse.osgi.util;version="1.1.0",
 org.osgi.framework;version="1.9.0",
 org.osgi.framework.namespace;version="1.1.0",
 org.osgi.framework.wiring;version="1.2.0",
 org.osgi.resource;version="1.0.0",
 org.osgi.service.log;version="1.4.0",
 org.osgi.service.packageadmin;version="1.2.0",
 org.osgi.util.tracker;version="1.5.2"
```

Because many Eclispe plugins, ask the *org.eclipse.core.runtime.Platform*, if it is running. We also added a system property called **@noeclipse** and additionally evaluate it in the *org.eclipse.core.internal.runtime.InternalPlatform#isRunning*, which *Platform#isRunning* delegates to.

## org.eclipse.equinox.supplement

The sad thing is the heavy use of the static methods *Platform#getBundle* and *Platform#getBundles* in *org.eclipse.core.runtime*. The implementation in the *InternalPlatform#getBundles* uses plain OSGi except for one thing. 

It is the **ModuleContainer#createRequirement** call in *InternalPlatform#getBundles*. The creation of the *org.osgi.resource.Requirement* is the only real hard link to the internal Equinox OSGi framework in bundle *org.eclipse.osgi*. 

```java
Map<String, String> directives = Collections.singletonMap(Namespace.REQUIREMENT_FILTER_DIRECTIVE,
				getRequirementFilter(symbolicName, versionRange));
		Collection<BundleCapability> matchingBundleCapabilities = fwkWiring.findProviders(ModuleContainer
				.createRequirement(IdentityNamespace.IDENTITY_NAMESPACE, directives, Collections.emptyMap()));

```

Without that dependency, we can substitute the *org.eclipse.osgi* with *org.eclipse.equinox.supplement*. This opens.

So we moved this call to *org.eclipse.equinox.supplement*. All code in the supplement bundle is shared from other bundles, to avoid duplicating code. 

Despite of that we created a new package *org.eclipse.osgi.container* and put our own smaller implementation in there.

We also export that package in the manifest:

```
...
Export-Package: org.eclipse.equinox.log;version="1.1",
 org.eclipse.osgi.container;version="1.5.0",
 org.eclipse.osgi.framework.console;version="1.1",
 org.eclipse.osgi.framework.eventmgr;version="1.2",
 ...
```

... and also import the required packages:

```
...
Import-Package: org.osgi.framework,
 org.osgi.framework.hooks.resolver,
 org.osgi.resource,
 org.osgi.service.log,
 org.osgi.service.resolver,
 org.osgi.util.tracker,
 org.eclipse.equinox.log,
 org.eclipse.osgi.framework.console,
 ...
```

## Artifacts

The corresponding patches can be found in the *patch* folder. There are also patched version available from our nexus:

https://devel.data-in-motion.biz/nexus/repository/dim-release/

```
<dependency>
  <groupId>org.eclipse.core</groupId>
  <artifactId>org.eclipse.core.runtime</artifactId>
  <version>3.16.100.201911011611</version>
</dependency>

<dependency>
  <groupId>org.eclipse.equinox</groupId>
  <artifactId>org.eclipse.equinox.supplement</artifactId>
  <version>1.9.100.201911011403</version>
</dependency>

<dependency>
  <groupId>org.gecko.eclipse</groupId>
  <artifactId>org.gecko.eclipse.core.supplement</artifactId>
  <version>1.0.0</version>
</dependency>
```





