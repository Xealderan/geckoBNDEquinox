package org.gecko.eclipse.compatibility;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.eclipse.core.runtime.internal.adaptor.EclipseAppLauncher;
import org.eclipse.osgi.framework.log.FrameworkLog;
import org.eclipse.osgi.internal.framework.EquinoxConfiguration;
import org.eclipse.osgi.service.environment.EnvironmentInfo;
import org.eclipse.osgi.service.runnable.ApplicationLauncher;
import org.gecko.eclipse.compatibility.equinox.config.api.EquinoxConfigInitializer;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.BundleException;
import org.osgi.framework.ServiceRegistration;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;
import org.osgi.service.component.annotations.Reference;
import org.osgi.service.component.annotations.ReferenceCardinality;
import org.osgi.service.component.annotations.ReferencePolicy;
import org.osgi.util.promise.Promise;
import org.osgi.util.promise.PromiseFactory;

@Component(immediate=true)
public class EclipsePlatformStarter {

	@Reference(policy=ReferencePolicy.STATIC, cardinality=ReferenceCardinality.MANDATORY)
	EquinoxConfigInitializer marker;
	
	@Reference
	FrameworkLog log;
	
	@Reference
	EnvironmentInfo envInfo;
	
	private ExecutorService executorService = Executors.newSingleThreadExecutor();

	private EclipseAppLauncher appLauncher;

	private ServiceRegistration<ApplicationLauncher> appLauncherRegistration;

	private Future<?> appLauncherRunnable;

	
	@Activate
	public void activate(BundleContext ctx) {
		
		Bundle runtime = findBundle("org.eclipse.core.runtime", ctx);
		if(runtime != null) {
			try {
				runtime.start();
			} catch (BundleException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		String ignoreApp = ctx.getProperty("eclipse.ignoreApp");
		if(!Boolean.parseBoolean(ignoreApp)) {
			PromiseFactory promiseFactory = new PromiseFactory(executorService);
			
			System.out.println("Registering Equinox App  Launcher");
//			boolean launchDefault = Boolean.valueOf(getProperty(PROP_APPLICATION_LAUNCHDEFAULT, "true")).booleanValue(); //$NON-NLS-1$
			// create the ApplicationLauncher and register it as a service
			appLauncher = new EclipseAppLauncher(ctx, false, false, log, (EquinoxConfiguration) envInfo);
			// must start the launcher AFTER service registration because this method 
			// blocks and runs the application on the current thread.  This method 
			// will return only after the application has stopped.
			System.out.println("Starting Equinox App Launcher");
			
			Promise<Object> applicationPromise = promiseFactory.submit(() -> {
				Object start = appLauncher.start(null);	
				return start;
			}).thenAccept(o -> {
				System.exit((Integer) o);
			});
			appLauncherRegistration = ctx.registerService(ApplicationLauncher.class, appLauncher, null);
			applicationPromise.onFailure(t -> t.printStackTrace()).thenAccept(o -> System.exit(42));
		}
	}
	
	@Deactivate
	public void deactivate() {
		appLauncherRunnable.cancel(true);
		appLauncherRegistration.unregister();
	}

	private Bundle findBundle(String bsn, BundleContext ctx) {
		Bundle[] bundles = ctx.getBundles();
		for(Bundle bundle : bundles) {
			if(bsn.equals(bundle.getSymbolicName())) {
				return bundle;
			}
		}
		return null;
	}
	
	

}
