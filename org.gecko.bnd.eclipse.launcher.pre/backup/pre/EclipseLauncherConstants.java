/**
 * Copyright (c) 2012 - 2019 Data In Motion and others.
 * All rights reserved. 
 * 
 * This program and the accompanying materials are made available under the terms of the 
 * Eclipse Public License v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors:
 *     Data In Motion - initial API and implementation
 */
package org.gecko.bnd.eclipse.launcher.pre;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.CodeSource;
import java.security.ProtectionDomain;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import org.gecko.bnd.eclipse.launcher.util.CommonUtil;

public class EclipseLauncherConstants {

	public static final String SPLASH_HANDLE = "org.eclipse.equinox.launcher.splash.handle"; //$NON-NLS-1$
	public static final String SPLASH_LOCATION = "org.eclipse.equinox.launcher.splash.location"; //$NON-NLS-1$
//	
//	//URLs
	public static final String PLATFORM_URL = "platform:/base/"; //$NON-NLS-1$
	public static final String FILE_SCHEME = "file:"; //$NON-NLS-1$    
//	public static final String REFERENCE_SCHEME = "reference:"; //$NON-NLS-1$
//	public static final String JAR_SCHEME = "jar:"; //$NON-NLS-1$
//	
//	// command line args
//	public static final String FRAMEWORK = "-framework"; //$NON-NLS-1$
//	public static final String INSTALL = "-install"; //$NON-NLS-1$
//	public static final String INITIALIZE = "-initialize"; //$NON-NLS-1$
//	public static final String VM = "-vm"; //$NON-NLS-1$
	public static final String VMARGS = "-vmargs"; //$NON-NLS-1$
	public static final String DEBUG = "-debug"; //$NON-NLS-1$
//	public static final String DEV = "-dev"; //$NON-NLS-1$
//	public static final String CONFIGURATION = "-configuration"; //$NON-NLS-1$
	public static final String NOSPLASH = "-nosplash"; //$NON-NLS-1$
	public static final String SHOWSPLASH = "-showsplash"; //$NON-NLS-1$
//	public static final String EXITDATA = "-exitdata"; //$NON-NLS-1$
	public static final String NAME = "-name"; //$NON-NLS-1$
	public static final String LAUNCHER = "-launcher"; //$NON-NLS-1$
//
//	public static final String PROTECT = "-protect"; //$NON-NLS-1$
//	//currently the only level of protection we care about.
//	public static final String PROTECT_MASTER = "master"; //$NON-NLS-1$
//	public static final String PROTECT_BASE = "base"; //$NON-NLS-1$
//
	public static final String LIBRARY = "--launcher.library"; //$NON-NLS-1$
//	public static final String APPEND_VMARGS = "--launcher.appendVmargs"; //$NON-NLS-1$
//	public static final String OVERRIDE_VMARGS = "--launcher.overrideVmargs"; //$NON-NLS-1$
	public static final String NL = "-nl"; //$NON-NLS-1$
	public static final String ENDSPLASH = "-endsplash"; //$NON-NLS-1$
	public static final String SPLASH_IMAGE = "splash.bmp"; //$NON-NLS-1$
	public static final String CLEAN = "-clean"; //$NON-NLS-1$
//	public static final String NOEXIT = "-noExit"; //$NON-NLS-1$
//	public static final String OS = "-os"; //$NON-NLS-1$
//	public static final String WS = "-ws"; //$NON-NLS-1$
//	public static final String ARCH = "-arch"; //$NON-NLS-1$
	public static final String STARTUP = "-startup"; //$NON-NLS-1$
//	
//	// constants: System property keys and/or configuration file elements
	public static final String PROP_USER_HOME = "user.home"; //$NON-NLS-1$
	public static final String PROP_USER_DIR = "user.dir"; //$NON-NLS-1$
	public static final String PROP_INSTALL_AREA = "osgi.install.area"; //$NON-NLS-1$
	public static final String PROP_CONFIG_AREA = "osgi.configuration.area"; //$NON-NLS-1$
	public static final String PROP_CONFIG_AREA_DEFAULT = "osgi.configuration.area.default"; //$NON-NLS-1$
//	public static final String PROP_BASE_CONFIG_AREA = "osgi.baseConfiguration.area"; //$NON-NLS-1$
//	public static final String PROP_SHARED_CONFIG_AREA = "osgi.sharedConfiguration.area"; //$NON-NLS-1$
//	public static final String PROP_CONFIG_CASCADED = "osgi.configuration.cascaded"; //$NON-NLS-1$
//	protected static final String PROP_FRAMEWORK = "osgi.framework"; //$NON-NLS-1$
//	public static final String PROP_CLASSPATH = "osgi.frameworkClassPath"; //$NON-NLS-1$
//	public static final String PROP_EXTENSIONS = "osgi.framework.extensions"; //$NON-NLS-1$
//	public static final String PROP_FRAMEWORK_SYSPATH = "osgi.syspath"; //$NON-NLS-1$
//	public static final String PROP_FRAMEWORK_SHAPE = "osgi.framework.shape"; //$NON-NLS-1$
//	public static final String PROP_LOGFILE = "osgi.logfile"; //$NON-NLS-1$
//	public static final String PROP_REQUIRED_JAVA_VERSION = "osgi.requiredJavaVersion"; //$NON-NLS-1$
//	public static final String PROP_PARENT_CLASSLOADER = "osgi.parentClassloader"; //$NON-NLS-1$
//	public static final String PROP_FRAMEWORK_PARENT_CLASSLOADER = "osgi.frameworkParentClassloader"; //$NON-NLS-1$
//	public static final String PROP_NL = "osgi.nl"; //$NON-NLS-1$
//	static final String PROP_NOSHUTDOWN = "osgi.noShutdown"; //$NON-NLS-1$
//	public static final String PROP_DEBUG = "osgi.debug"; //$NON-NLS-1$	
//	public static final String PROP_OS = "osgi.os"; //$NON-NLS-1$
//	public static final String PROP_WS = "osgi.ws"; //$NON-NLS-1$
//	public static final String PROP_ARCH = "osgi.arch"; //$NON-NLS-1$
//
//	public static final String PROP_EXITCODE = "eclipse.exitcode"; //$NON-NLS-1$
//	public static final String PROP_EXITDATA = "eclipse.exitdata"; //$NON-NLS-1$
//
//	public static final String PROP_VM = "eclipse.vm"; //$NON-NLS-1$
//	public static final String PROP_VMARGS = "eclipse.vmargs"; //$NON-NLS-1$
//	public static final String PROP_COMMANDS = "eclipse.commands"; //$NON-NLS-1$
//	public static final String PROP_ECLIPSESECURITY = "eclipse.security"; //$NON-NLS-1$
	
	public static final String PROP_LAUNCHER = "eclipse.launcher"; //$NON-NLS-1$
	public static final String PROP_LAUNCHER_NAME = "eclipse.launcher.name"; //$NON-NLS-1$

	public static final String PROP_SPLASHPATH = "osgi.splashPath"; //$NON-NLS-1$

	/**
	 * The direct location of the splash bmp file
	 */
	public static final String PROP_SPLASHLOCATION = "-osgi.splashLocation"; //$NON-NLS-1$
	
	/**
	 * The ID of the Launcher Bundle
	 */
	public static final String BUNDLE_ID = "org.gecko.bnd.eclipse.launcher";
	
	public boolean debug = false;
	public boolean splashDown = false;
	public boolean initialize = false;
	public boolean showSplash = true;
	public String splashLocation;
	public String splashPath;
	public String[] vmargs;
	public String[] commands;
	public String framework;
	public String exitData;
	public String library;
	public String vm;
	public String endSplash;
	public String nl;
	public String configArea;
	public URL installationLocation;

	//Added by jalbert
	//// should be followed by a coma separated list of jar locations. urls can be relative to the install location or as absolut urls.
	public static final String	PROP_RUNPATH	= "-runpath";
	public static final String	LAUNCH_TRACE		= "launch.trace";

	//BND colelcts its classpath for the actuall Launcher from an entry in the Manifest and expects everything to be in the executable jar or somehwere close by.
	//This extra classpath should allow for some more flexibility 
	public List<String> propBasedRunPath = new LinkedList<>();
	
	public EclipseLauncherConstants(String[] args) {
		debug = Boolean.getBoolean(LAUNCH_TRACE);
		commands = args;
		int[] configArgs = new int[args.length];
		configArgs[0] = -1; // need to initialize the first element to something that could not be an index.
		int configArgIndex = 0;
		for (int i = 0; i < args.length; i++) {
			boolean found = false;
			// check for args without parameters (i.e., a flag arg)
			// check if debug should be enabled for the entire platform
			if (args[i].equalsIgnoreCase(DEBUG)) {
				debug = true;
				// passed thru this arg (i.e., do not set found = true)
				continue;
			}

			// look for and consume the nosplash directive.  This supercedes any
			// -showsplash command that might be present.
			if (args[i].equalsIgnoreCase(NOSPLASH)) {
				splashDown = true;
				found = true;
			}

			// look for the command to use to show the splash screen
			if (args[i].equalsIgnoreCase(SHOWSPLASH)) {
				showSplash = true;
				found = true;
				//consume optional parameter for showsplash
				if (i + 1 < args.length && !args[i + 1].startsWith("-")) { //$NON-NLS-1$
					configArgs[configArgIndex++] = i++;
					splashLocation = args[i];
				}
			}

			// look for the command to use to show the splash screen
//			if (args[i].equalsIgnoreCase(PROTECT)) {
//				found = true;
//				//consume next parameter
//				configArgs[configArgIndex++] = i++;
//				if (args[i].equalsIgnoreCase(PROTECT_MASTER) || args[i].equalsIgnoreCase(PROTECT_BASE)) {
//					protectBase = true;
//				}
//			}

			// done checking for args.  Remember where an arg was found 
			if (found) {
				configArgs[configArgIndex++] = i;
				continue;
			}

			// look for the VM args arg.  We have to do that before looking to see
			// if the next element is a -arg as the thing following -vmargs may in
			// fact be another -arg.
			if (args[i].equalsIgnoreCase(VMARGS)) {
				// consume the -vmargs arg itself
				args[i] = null;
				i++;
				vmargs = new String[args.length - i];
				for (int j = 0; i < args.length; i++) {
					vmargs[j++] = args[i];
					args[i] = null;
				}
				continue;
			}

			// check for args with parameters. If we are at the last argument or if the next one
			// has a '-' as the first character, then we can't have an arg with a parm so continue.
			if (i == args.length - 1 || args[i + 1].startsWith("-")) //$NON-NLS-1$
				continue;
			String arg = args[++i];

			// look for the name to use by the launcher
			if (args[i - 1].equalsIgnoreCase(NAME)) {
				System.getProperties().put(PROP_LAUNCHER_NAME, arg);
				found = true;
			}

			// look for the startup jar used 
			if (args[i - 1].equalsIgnoreCase(STARTUP)) {
				//not doing anything with this right now, but still consume it
				//startup = arg;
				found = true;
			}

			// look for the launcher location
			if (args[i - 1].equalsIgnoreCase(LAUNCHER)) {
				//not doing anything with this right now, but still consume it
//				launcher = arg;
				System.getProperties().put(PROP_LAUNCHER, arg);
				found = true;
			}

			
			//XXX MANDATORY
			if (args[i - 1].equalsIgnoreCase(LIBRARY)) {
				library = arg;
				found = true;
			}

			//XXX MANDATORY
			if (args[i - 1].equalsIgnoreCase(PROP_RUNPATH)) {
				String runPath = arg;
				propBasedRunPath.addAll(Arrays.asList(runPath.split(",")));
				found = true;
			}

			//XXX MANDATORY if A Splashscreen needs showing
			if (args[i - 1].equalsIgnoreCase(PROP_SPLASHLOCATION)) {
				splashLocation = arg;
				found = true;
			}

			//XXX if no Splashlocation is set, we might use a SplashPath
			if (args[i - 1].equalsIgnoreCase(PROP_SPLASHPATH)) {
				splashPath = arg;
				found = true;
			}

			//XXX the language setting used to determine the splash image  
			if (args[i - 1].equalsIgnoreCase(NL)) {
				nl = arg;
				found = true;
			}

			
			
			if (args[i - 1].equalsIgnoreCase(NL)) {
				nl = arg;
				found = true;
			}

			if (args[i - 1].equalsIgnoreCase(PROP_CONFIG_AREA)) {
				configArea = arg;
				found = true;
			}

			// look for the command to use to end the splash screen
			if (args[i - 1].equalsIgnoreCase(ENDSPLASH)) {
				endSplash = arg;
				found = true;
			}

			// done checking for args.  Remember where an arg was found 
			if (found) {
				configArgs[configArgIndex++] = i - 1;
				configArgs[configArgIndex++] = i;
			}
		}
		// remove all the arguments consumed by this argument parsing
		String[] passThruArgs = new String[args.length - configArgIndex - (vmargs == null ? 0 : vmargs.length + 1)];
		configArgIndex = 0;
		int j = 0;
		for (int i = 0; i < args.length; i++) {
			if (i == configArgs[configArgIndex])
				configArgIndex++;
			else if (args[i] != null)
				passThruArgs[j++] = args[i];
		}
		
		installationLocation = getInstallLocation();
		splashLocation = System.getProperty(EclipseLauncherConstants.PROP_SPLASHLOCATION, splashLocation);
		splashPath = System.getProperty(EclipseLauncherConstants.PROP_SPLASHPATH, splashPath);
		configArea = System.getProperty(EclipseLauncherConstants.PROP_CONFIG_AREA, configArea);
	}
	
	/**
	 * Returns url of the location this class was loaded from
	 */
	private URL getInstallLocation() {
		URL installLocation = null;
		// value is not set so compute the default and set the value
		String installArea = System.getProperty(PROP_INSTALL_AREA);
		if (installArea != null) {
			installLocation = CommonUtil.buildURL(installArea, true);
			if (installLocation == null)
				throw new IllegalStateException("Install location is invalid: " + installArea); //$NON-NLS-1$
			System.setProperty(PROP_INSTALL_AREA, installLocation.toExternalForm());
			if (debug)
				System.out.println("Install location:\n    " + installLocation); //$NON-NLS-1$
			return installLocation;
		}

		ProtectionDomain domain = EclipseStyleEmbeddedLauncher.class.getProtectionDomain();
		CodeSource source = null;
		URL result = null;
		if (domain != null)
			source = domain.getCodeSource();
		if (source == null || domain == null) {
			if (debug)
				System.out.println("CodeSource location is null. Defaulting the install location to file:startup.jar"); //$NON-NLS-1$
			try {
				result = new URL("file:startup.jar"); //$NON-NLS-1$
			} catch (MalformedURLException e2) {
				//Ignore
			}
		}
		if (source != null)
			result = source.getLocation();

		String path = CommonUtil.decode(result.getFile());
		// normalize to not have leading / so we can check the form
		File file = new File(path);
		path = file.toString().replace('\\', '/');
		// TODO need a better test for windows
		// If on Windows then canonicalize the drive letter to be lowercase.
		// remember that there may be UNC paths 
		if (File.separatorChar == '\\')
			if (Character.isUpperCase(path.charAt(0))) {
				char[] chars = path.toCharArray();
				chars[0] = Character.toLowerCase(chars[0]);
				path = new String(chars);
			}
		if (path.toLowerCase().endsWith(".jar")) //$NON-NLS-1$
			path = path.substring(0, path.lastIndexOf('/') + 1); //$NON-NLS-1$
		if (path.toLowerCase().endsWith("/plugins/")) //$NON-NLS-1$ 
			path = path.substring(0, path.length() - "/plugins/".length()); //$NON-NLS-1$
		try {
			try {
				// create a file URL (via File) to normalize the form (e.g., put 
				// the leading / on if necessary)
				path = new File(path).toURL().getFile();
			} catch (MalformedURLException e1) {
				// will never happen.  The path is straight from a URL.  
			}
			installLocation = new URL(result.getProtocol(), result.getHost(), result.getPort(), path);
			System.setProperty(PROP_INSTALL_AREA, installLocation.toExternalForm());
		} catch (MalformedURLException e) {
			// TODO Very unlikely case.  log here.  
		}
		if (debug)
			System.out.println("Install location:\n    " + installLocation); //$NON-NLS-1$
		return installLocation;
	}
	
//	/**
//	 * Returns the <code>URL</code>-based class path describing where the boot classes are located.
//	 * 
//	 * @return the url-based class path
//	 * @param base the base location
//	 * @exception MalformedURLException if a problem occurs computing the class path
//	 */
//	protected URL getBootPath(String base) throws IOException {
//		URL url = null;
//		if (base != null) {
//			url = CommonUtil.buildURL(base, true);
//		} else {
//			// search in the root location
//			url = getInstallLocation();
//			String path = new File(url.getFile(), "plugins").toString(); //$NON-NLS-1$
//			path = CommonUtil.searchFor(framework, path);
//			if (path == null)
//				throw new RuntimeException("Could not find framework"); //$NON-NLS-1$
//			if (url.getProtocol().equals("file")) //$NON-NLS-1$
//				url = new File(path).toURL();
//			else
//				url = new URL(url.getProtocol(), url.getHost(), url.getPort(), path);
//		}
//		if (System.getProperty(PROP_FRAMEWORK) == null)
//			System.getProperties().put(PROP_FRAMEWORK, url.toExternalForm());
//		if (debug)
//			CommonUtil.log(EclipseLauncherConstants.class, "Framework located:\n    " + url.toExternalForm()); //$NON-NLS-1$
//		return url;
//	}
	
}
