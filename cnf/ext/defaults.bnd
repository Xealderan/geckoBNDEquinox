java: java
javac: javac
javac.source: 1.8
javac.target: 1.8
javac.debug:  on

#Bundle-DocURL: https://yourdoc.de
Bundle-License: Eclipse Public License 1.0
Bundle-Copyright:Data In Motion GmbH all rights reserved
Bundle-Vendor: Data In Motion GmbH
Bundle-ContactAddress: info@data-in-motion.biz
Bundle-Icon: icons/gecko.ico;size=64

#if you want bnd to tell you more during the build
#-verbose: true

# Includes sources from bundle
-sources: true

-includeresource.license: \
	META-INF/LICENSE=${workspace}/cnf/license/LICENSE

-includeresource.icon: \
	icons=${workspace}/cnf/icons
		
-removeheaders: Bnd-LastModified, Tool, Created-By

# Path to ...
#cnf: ${workspace}/cnf

# This is the version of JUnit that will be used at build time and runtime
geckotest: org.gecko.core.test;version="[1.0.0,2.0.0)"
# This is the version of JUnit that will be used at build time and runtime
junit: org.apache.servicemix.bundles.junit;version="[4.11,5)"
# This is the version of Mockito that will be used at build time and run time
mockito: org.mockito.mockito-core;version="[1.9,2)",\
  org.objenesis;version="[2.1,3)"

# String to substitute for "SNAPSHOT" in the bundle version's qualifier.
#-snapshot: ${tstamp}
#-buildrepo: Local

-plugin.baseline: \
	aQute.bnd.repository.osgi.OSGiRepository;\
		name=Baseline;\
		readonly = true;\
		max.stale=-1;\
		locations=https://devel.data-in-motion.biz/repository/gecko/release/geckoBNDEquinox/index.xml;\
		cache=${build}/cache/Baseline

# Enable semantic versioning for all bundles
-baselinerepo: Baseline
-baseline: *

-maven-release: pom

# Provide a useful group-id
-groupid: org.gecko.bnd.equinox

-releaserepo: Release, DIM_Release

# Ignore files for baselining
-diffignore: *.xml,\
    */pom.properties,\
     OSGI-OPT/*

# define global blacklist
-runblacklist.default: osgi.identity;filter:='(osgi.identity=osgi.cmpn)'

-testpath: ${junit},\
  ${mockito}

-resolve.effective: active
#-resolve.effective: active;skip:="osgi.service"

# > Java 8 runtime
#modules: --add-opens=java.base/jdk.internal.loader=ALL-UNNAMED, --add-opens=java.base/java.lang=ALL-UNNAMED, --add-opens=java.base/java.net=ALL-UNNAMED, --add-opens=java.base/java.security=ALL-UNNAMED
#-runvm.default: ${if;${isempty;${system_allow_fail;java --list-modules}};;${modules}}

-include: ${if;${def;release.dir};\
              ${cnf}/releng/release.bnd;\
              ${cnf}/releng/snapshot.bnd\
          }